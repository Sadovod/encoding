from tkinter import *

import time
import random
import backend


PIN_CODE_TRANSFORMATIONS = ["+1", "-1", "*2", "*3"]
TRANSFORMED_PIN_CODE = []

root = Tk()
root.title("Encoder")

main_frame = Frame(root)
label_frame = Frame(root)
input_frame = Frame(root)
info_frame = Frame(root)

label_frame.pack()
main_frame.pack()

"""КНОПКИ ВВОДА PIN КОДА"""

ENTERED_CODE = []

btn_numbers = list(range(10))


def update_buttons():
    random.shuffle(btn_numbers)
    r = 1
    c = 0
    for i in btn_numbers:
        btn = Button(main_frame, text=i, command=lambda x=i: button_click(x), width=10).grid(row=r, column=c)
        c += 1
        if c > 2:
            c = 0
            r += 1
        if r > 3:
            c = 1


def button_click(number):
    TRANSFORMED_PIN_CODE.append(eval(f"{btn_numbers[backend.PIN_CODE[len(ENTERED_CODE)]-1]} {PIN_CODE_TRANSFORMATIONS[len(ENTERED_CODE)]}") % 10)
    ENTERED_CODE.append(number)
    count_label = Label(label_frame, text=f"Введено {len(ENTERED_CODE)} из 4 цифр", width=30).grid(row=0, column=1, columnspan=3)
    if len(ENTERED_CODE) == 4:
        # if ENTERED_CODE == TRANSFORMED_PIN_CODE:
        if ENTERED_CODE:
            ENTERED_CODE.clear()
            TRANSFORMED_PIN_CODE.clear()
            open_input()
            return
        else:
            ENTERED_CODE.clear()
            TRANSFORMED_PIN_CODE.clear()
            error_label = Label(label_frame, text="Неверный код", foreground="#ff0000", width=30).grid(row=0, column=1, columnspan=3)
    update_buttons()


def enter_click(text):
    input_frame.destroy()
    info_frame.pack()

    text = "ф" * 1000

    enc_start = time.time()
    encoded_bits = backend.encoding(text)  # информация о кодировании
    enc_end = time.time()

    print(enc_end - enc_start)

    enc_start = time.time()
    encoded_bits = backend.encoding(text)  # информация о кодировании
    enc_end = time.time()

    print(enc_end - enc_start)

    dec_start = time.time()
    decoding_data = backend.decoding(encoded_bits)  # информация о декодировании
    dec_end = time.time()

    print(dec_end - dec_start)

    # encoding_time = Label(info_frame, text=f"Время кодирования: {encoding_data['encoding_time']}").grid(row=1, pady=10)
    # encoded_bits_count = Label(info_frame, text=f"Количество битов: {encoding_data['bits_count']}").grid(row=2, pady=10)
    # decoded_text = Label(info_frame, text=f"Декодированный текст: {decoding_data['decoded_text']}").grid(row=3, pady=10)
    # decoding_time = Label(info_frame, text=f"Время декодирования: {decoding_data['decoding_time']}").grid(row=4, pady=10)


def open_input():
    main_frame.destroy()
    label_frame.destroy()
    input_frame.pack()
    Label(input_frame, text="Введите текст", width=30).grid(row=0, column=1, columnspan=3)
    entered_text = StringVar()  # содержит в себе введенный текст
    text_field = Entry(input_frame, textvariable=entered_text, width=30).grid(row=1, column=0, columnspan=3)
    enter = Button(input_frame, text="Принять", command=lambda: enter_click(entered_text.get()), width=10).grid(row=3, column=1, pady=10)


if __name__ == "__main__":
    update_buttons()
    root.mainloop()
