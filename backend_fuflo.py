from numba import jit
import numpy as np
import math


PIN_CODE = [1, 7, 8, 3]


def formula_run(x):
    k = PIN_CODE
    result = x[0] * (((x[1] + x[2])**k[0] * x[3] + math.sin(x[4]) + k[1]) / (math.log(x[6], x[5]) - x[7]**k[2] + math.cos(x[8]) - x[9]**(1/k[3])))
    return result


def get_bit(value, idx):
    return (value >> (idx - 1)) & 1


# Кодирование символов Unicode
@jit(nopython=True)
def encoding(some_nums):
    encoded_bits = 1
    bits_num = 16

    k = [1, 7, 8, 3]
    x_all = np.random.randint(2, 10, (bits_num * len(some_nums), 10))
    x_index = 0  # индекс списка для функции formula_run


    for idx, num in enumerate(some_nums):
        for j in range(bits_num):
            i = bits_num - 1 - j
            x = x_all[x_index]
            formula_result = x[0] * (((x[1] + x[2])**k[0] * x[3] + math.sin(x[4]) + k[1]) / (math.log(x[6])/math.log(x[5]) - x[7]**k[2] + math.cos(x[8]) - x[9]**(1/k[3])))
            x_index += 1

            i_bit = (num >> i) & 1 # get bit with i index
            if (formula_result % 2) == 0:
                encoded_bits = (encoded_bits << 1) | i_bit
            else:
                encoded_bits = (encoded_bits << 1) | abs(i_bit - 1) # reverse bit

            for r_numb in x:
                encoded_bits = (encoded_bits << 4) | r_numb

    return encoded_bits


@jit(nopython=True)
def decoding(encoded_bits):
    decoded_symbols_bits = ""
    for i in range(0, len(encoded_bits), 41):
        symbol_bit_with_random_numbers = encoded_bits[i:i+41]  # 41 -> 1 бит + 4 бит * 10
        symbol_bit = symbol_bit_with_random_numbers[0]
        random_numbers_bits = symbol_bit_with_random_numbers[1:]

        x = []  # список 10 случайных чисел
        for n in range(0, len(random_numbers_bits), 4):
            x.append(int(random_numbers_bits[n:n+4], base=2))

        # Восстановление битов с помощью формулы
        formula_result = int(str(formula_run(x))[-1])
        if (formula_result % 2) == 0:
            decoded_symbols_bits += str(abs(int(i) - 1))
        else:
            decoded_symbols_bits += symbol_bit

    decoded_text = ""
    for i in range(0, len(decoded_symbols_bits), 8):
        one_symbol_bits = decoded_symbols_bits[i:i+8]
        decoded_text += chr(int(one_symbol_bits[one_symbol_bits.find("1"):], base=2))

    return decoded_text


# if (int(str(formula_result)[-1]) % 2) == 0:
#     encoded_bits.append() += str(abs(i - 1))  # Меняет бит если результат формулы четный
# else:
#     encoded_bits.append() += i
#
# for number in x:    # к каждому биту добавляем 10 случайных чисел
#     encoded_bits += str(bin(number)[2:]).zfill(4)  # каждое число будет занимать 4 бит
