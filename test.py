from backend_fuflo import *
from backend import encoding as str_encoding
from time import time
import warnings
warnings.filterwarnings('ignore')

from_ = time()
encoding([1])
print (f'Compilation of numba func took {time() - from_}')


arr_len = 10000
test_text = '1' * arr_len
test_nums = [ord(i) for i in test_text]

from_ = time()
encoding(test_nums)
print (f'Numba call with array of length {arr_len} took {time() - from_}')

from_ = time()
str_encoding(test_text)
print (f'Bad call with array of length {arr_len} took {time() - from_}')
