import numpy as np
import math


PIN_CODE = [1, 7, 8, 3]


def formula_run(x, k):
    result = x[0] * (((x[1] + x[2])**k[0] * x[3] + math.sin(x[4]) + k[1]) / (math.log(x[6], x[5]) - x[7]**k[2] + math.cos(x[8]) - x[9]**(1/k[3])))
    return result


# Кодирование символов Unicode
def encoding(some_text):
    encoded_bits = ""

    k = PIN_CODE
    x_all = np.random.randint(2, 10, (16 * len(some_text), 10))
    x_index = 0  # индекс списка для функции formula_run

    for symbol in some_text:
        symbol_bits = str(bin(ord(symbol))[2:]).zfill(16)

        for i in symbol_bits:
            x = x_all[x_index]
            formula_result = formula_run(x, PIN_CODE)
            x_index += 1

            if (int(str(formula_result)[-1]) % 2) == 0:
                encoded_bits += str(abs(int(i) - 1))  # Меняет бит если результат формулы четный
            else:
                encoded_bits += i

            for number in x:    # к каждому биту добавляем 10 случайных чисел
                encoded_bits += str(bin(number)[2:]).zfill(4)  # каждое число будет занимать 4 бит

    return encoded_bits


def decoding(encoded_bits):
    decoded_symbols_bits = ""
    for i in range(0, len(encoded_bits), 41):
        symbol_bit_with_random_numbers = encoded_bits[i:i+41]  # 41 -> 1 бит + 4 бит * 10
        symbol_bit = symbol_bit_with_random_numbers[0]
        random_numbers_bits = symbol_bit_with_random_numbers[1:]

        x = []  # список 10 случайных чисел
        for n in range(0, len(random_numbers_bits), 4):
            x.append(int(random_numbers_bits[n:n+4], base=2))

        # Восстановление битов с помощью формулы
        formula_result = int(str(formula_run(x, PIN_CODE))[-1])
        if (formula_result % 2) == 0:
            decoded_symbols_bits += str(abs(int(i) - 1))
        else:
            decoded_symbols_bits += symbol_bit

    decoded_text = ""
    for i in range(0, len(decoded_symbols_bits), 16):
        one_symbol_bits = decoded_symbols_bits[i:i+16]
        decoded_text += chr(int(one_symbol_bits[one_symbol_bits.find("1"):], base=2))

    return decoded_text
